package server

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"net/http"
)

func (s *Server) doesUserExist(username string) (bool, error) {
	user := new(User)
	err := s.DBClient.Database("users").Collection("users").FindOne(context.Background(), bson.M{"username": username}).Decode(user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func writeWrapper(status int, message string, w http.ResponseWriter) {
	log.Printf("%s: %d", message, status)
	w.WriteHeader(status)
	_, err := w.Write([]byte(message))
	if err != nil {
		log.Printf("Failed to write: %s\n", err.Error())
	}
}

func makeMessageResponse(message string, err error) string {
	if err != nil {
		return fmt.Sprintf(`{"error": "%s: %v"}`, message, err)
	}
	return fmt.Sprintf(`{"success": "%s"}`, message)
}
